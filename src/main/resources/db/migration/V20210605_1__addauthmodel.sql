drop table if exists auth;
create table auth
(
    id serial,
    login varchar(64),
    password varchar(64),
    last_login_timestamp bigint,
    user_id bigint,
    token uuid
);