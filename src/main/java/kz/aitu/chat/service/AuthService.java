package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private UsersService usersService;
    private AuthRepository authRepository;

    public UUID Login(String login, String password) throws Exception {
        Auth authModel = authRepository.findFirstByLoginAndPassword(login, password);
        if (authModel == null) throw new Exception("Invalid login or password");
        return authModel.getToken();
    }

    public UUID Register(String login, String password, String name) throws Exception {
        Users newUser = new Users();
        newUser.setName(name);
        Long userId = usersService.addUser(newUser);
        Auth authModel = authRepository.findFirstByLogin(login);
        if (authModel != null) throw new Exception("Username already exists!");
        if(password.length() < 5 && password.isBlank()) throw new Exception("Invalid password!");
        Auth auth = new Auth();
        auth.setLogin(login);
        auth.setPassword(password);
        auth.setUserId(userId);
        authRepository.save(auth);
        return auth.getToken();
    }

    public boolean validateToken(UUID token){
        Auth auth = authRepository.findFirstByToken(token);
        if (auth != null) return true;
        else return false;
    }

    public Long getIdByToken(UUID token){
        Auth auth = authRepository.findFirstByToken(token);
        if (auth != null) return auth.getUserId();
        else return null;
    }
}
