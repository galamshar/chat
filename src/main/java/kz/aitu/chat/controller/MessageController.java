package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {
    private MessageService messageService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(messageService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(messageService.findById(id));
    }

    @GetMapping("/{chat_id}/chat")
    public ResponseEntity<?> getLast10MessagesByChatId(@PathVariable Long chat_id, @RequestHeader("token") UUID token) throws Exception {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long reader_id = authService.getIdByToken(token);
        if (reader_id == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(messageService.getLast10MessagesByChatId(chat_id, reader_id));
    }

    @GetMapping("/{user_id}/user")
    public ResponseEntity<?> getLast10MessagesByUserId(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long user_id = authService.getIdByToken(token);
        if (user_id == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(messageService.getLast10MessagesByUserId(user_id));
    }

    @PostMapping("/edit/{message_id}")
    public ResponseEntity<?> editMessage(@RequestBody Message message, @RequestHeader("token") UUID token) throws Exception {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long user_id = authService.getIdByToken(token);
        if (user_id == null || message.getUserId() != user_id)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        try {
            messageService.update(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Message updated");

    }

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message, @RequestHeader("token") UUID token) throws Exception {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long user_id = authService.getIdByToken(token);
        if (user_id == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        try {
            message.setUserId(user_id);
            messageService.addMessage(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Message added");

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        try {
            messageService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("Message not found or deleted " + id);
        }
        return ResponseEntity.ok("Message successfully Deleted:");

    }

    @GetMapping("/{chatId}/unread")
    public ResponseEntity<?> getUnDeliveredMessages(@PathVariable Long chatId, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long userId = authService.getIdByToken(token);
        if (userId == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        try {
            return ResponseEntity.ok(messageService.getAllNotDeliveredMessagesByChatId(chatId, userId));
        } catch (Exception e) {
            return ResponseEntity.ok("Error! Try again. ");
        }
    }
}
