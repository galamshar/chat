package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
@RequestMapping("api/v1/users")
@AllArgsConstructor
public class UsersController {
    private UsersService usersService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(usersService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(usersService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addUser(@RequestBody Users user, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(usersService.save(user));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        usersService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{userId}/chats")
    public ResponseEntity<?> getAllChatsByUserId(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long userId = authService.getIdByToken(token);
        if (userId == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(usersService.getAllChatsByUserId(userId));
    }


}
