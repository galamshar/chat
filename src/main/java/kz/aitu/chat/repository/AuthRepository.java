package kz.aitu.chat.repository;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AuthRepository extends JpaRepository<Auth, Long> {
    public Auth findFirstByLoginAndPassword(String login, String password);
    public Auth findFirstByLogin(String login);
    public Auth findFirstByToken(UUID token);
}
