package kz.aitu.chat.repository;


import kz.aitu.chat.model.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findFirst10ByChatIdOrderByIdDesc(Long chatId);

    List<Message> findFirst10ByUserIdOrderByIdDesc(Long userId);

    List<Message> findAllByChatId(Long chatId, Pageable pageable);

    List<Message> findAllByChatIdAndAndUserIdIsNotAndIsDeliveredTrueAndIsReadFalse(Long chatId, Long userId, Pageable pageable);
    List<Message> findAllByChatIdAndAndUserIdIsNotAndIsDeliveredFalse(Long chatId, Long userId);

}